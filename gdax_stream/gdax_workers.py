from gdax_stream.order_book_new import GDAXQueuer
import queue
import time
import datetime as dt
from settings.constants import Side, DATE_FMT
import pickle
from utils.io import DUMPER


MIN_TIME_RESOLUTION = 1  # seconds


def gdax_timeliner(queue, order_book, timeline, file_obj):
    item = queue.get()
    result = None
    message = item['message']
    order_id = get_order_id(message)
    timestamp = message['time']
    message_is_valid = is_valid_message(message, order_book)
    on_book = order_id in order_book
    if message_is_valid:
        if on_book:
            time_from_last_update = not is_last_update_time(message,
                                                            order_book)
            if time_from_last_update:
                result = append_processed_message(item,
                                                  timeline, order_book)
                DUMPER(file_obj, result)
            update_order_book(message, order_book)
        if not on_book:
            insert_order_in_book(message, order_book)
    queue.task_done()


def get_order_id(message):
    " maker_order_id -> order_id on match message """
    
    msg_type = message['type']
    order_id = message.get('maker_order_id') \
               if msg_type == 'match' else message.get('order_id')
    return order_id


def is_valid_message(message, order_book):
    """ Return True if message is valid """

    if message['type'] == 'received':
        return False
    order_id = get_order_id(message)
    if message['type'] == 'open':
        size = message.get('remaining_size')
    if message['type'] == 'match':
        if not(order_id in order_book):
            return False        
        size = message.get('size')
    if message['type'] == 'change':
        if not(order_id in order_book):
            return False
        size = message.get('new_size')        
    if message['type'] == 'done':
        if not(order_id in order_book):
            return False       
        size = message.get('remaining_size')
    price = message.get('price')
    if (size is None) or (price is None):  # discard
        return False
    return True



def append_processed_message(item, timeline, order_book):
    """ 
    Append (start, end, price, size, side, market) to timeline

    """

    market = item['market']
    currency_pair = item['currency_pair']
    message = item['message']
    order_id = get_order_id(message)
    last_update = order_book.get(order_id)
    start = last_update['time']
    end = unix_from_isoformat(message['time'])
    price = last_update['price']
    side = message['side']
    side = Side[side].value
    size = last_update['size']
    new_element = (start, end, price, size, side, market, currency_pair)
    timeline.append(new_element)
    return new_element


def is_last_update_time(message, order_book):
    order_id = get_order_id(message)
    message_time = unix_from_isoformat(message['time'])
    last_update_time = order_book[order_id].get('time')
    if message_time - last_update_time < MIN_TIME_RESOLUTION:
        return True
    return False
   

def insert_order_in_book(open_message, order_book):
    order_id = open_message.get('order_id')
    order_book[order_id] = {
        "time": unix_from_isoformat(open_message["time"]),
        "size": float(open_message["remaining_size"]),
        "price": float(open_message["price"]),
        "side": open_message["side"]
        }


def update_order_book(message, order_book):
    """ Update order book """

    order_id = get_order_id(message)
    last_update = order_book.get(order_id)
    if message['type'] == 'match':
        size_change = float(message.get('size'))
        last_size = float(last_update.get('size'))
        size = last_size - size_change
    if message['type'] == 'change':
        size = float(message.get('new_size'))
    if message['type'] == 'done':
        del order_book[order_id]
        return
    if not size:
        del order_book[order_id]
    else:
        order_book[order_id] = {
            "time": unix_from_isoformat(message["time"]),
            "size": size,
            "price": float(message["price"]),
            "side": message["side"]
            }


def unix_from_isoformat(iso_utc):
    """ 
    iso format UTC (final Z) -> unix time format 
    
    iso_utc must end with Z in order to be processed
    """

    utc_time = dt.datetime.strptime(iso_utc, DATE_FMT)
    # In Python 3 the following can be substituted by
    # timestamp() method of datetime class (utc_time.timestamp())
    unix_time = (utc_time - dt.datetime(1970, 1, 1)).total_seconds()
    unix_time = int(round(unix_time))
    return unix_time

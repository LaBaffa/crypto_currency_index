import gdax
import json

class WebsocketCustom(gdax.WebsocketClient):
    """ Class to do custom actions when connected to gdax websocket """
    
    def __init__(self):
        super(WebsocketCustom, self).__init__()
        self.events = [] # list of received events

    # Customized methods of WebsocketClient. Do your change here!
    def on_open(self):
        self.url = "wss://ws-feed.gdax.com/"
        self.products = ["BTC-USD"]
        print("Welcome to your personal client!")

        
    def on_message(self, msg):
        """ Store event in self.event and print a string of it"""
        event = msg
        event_string = json.dumps(msg, indent=4, sort_keys=True)
        self.events.append(event)
        print(event_string)


    def on_close(self):
        print("Au revoir!")

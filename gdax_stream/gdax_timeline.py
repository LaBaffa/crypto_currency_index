import datetime as dt
import dateutil.parser
from collections import defaultdict


class GDAXTimeline(object):
    """ 
    Class to store messages from gdax market streamed from 
    the full_channel channel (see gdax docs) in different formats

    aggregated_orders -> dict of aggregated bids/asks during time
    single_orders     -> list of tuples representing single orders 
                         in specific time intervals

    """
    def __init__(self, gdax_fullchannel_list):
        self.messages = gdax_fullchannel_list

    
    def aggregated_orders(self):
        """ 
        Return a dictionary of dictionaries (a tree) divided by 
        bids and asks, for each price present in the 
        message list, a time evolution of their sizes (a cumulative 
        size over all active orders) is tracked.
        Will look like this:
        GDAXTimeline.aggregated_orders = {
            'bids':{
                price1:{
                    time1_1: size1_1,
                    time1_2: size1_2,
                    ... : ...  ,
                    time1_N1: size1_N1
                }
                price2:{
                    time2_1: size2_1,
                    time2_2: size2_2,
                    ... : ...  ,
                    time2_N2: size2_N2
                }
                ...  :{...}
                priceM:{
                    timeM_1: sizeM_1,
                    timeM_2: sizeM_2,
                    ... : ...  ,
                    timeM_NM: sizeM_NM
                }
                'asks':{
                    same structure of 'bids'
                }
            }

        """
        self.incomplete_aggregated = {'bids': {}, 'asks': {}}
        # Loop on messages and decide what to do based on message type
        for message in self.messages:
            msg_type = message['type']
            if msg_type == 'open':
                self.add_to_aggregated(message)
            elif msg_type == 'done' and 'price' in message:
                self.remove_from_aggregated(message)
            elif msg_type == 'match':
                self.match_aggregated(message)  
            elif msg_type == 'change':
                self.change_aggregated(message)
        return self.incomplete_aggregated

    def add_to_aggregated(self, order):
        """ Dealing with 'open' message """

        # Define variables
        record = {
            'side': order['side'],
            'price': order['price'],
            'size': float(order['remaining_size']),
            'time': order['time']
        }
        side = record['side']
        price = record['price']
        timestamp = record['time']

        # Get item in the aggregated timeline dict
        # with same side and price, if any
        if side == 'buy':
            line_side = 'bids'
            price_history = self.aggregated_bid_history(price)
        else:
            line_side = 'asks'
            price_history = self.aggregated_ask_history(price)

        if not(price_history): # if order price not yet in timeline
            self.incomplete_aggregated[line_side][price] = \
                                {timestamp:record['size']}
        else: # if already in timeline we sum to most recent size
            size = self.get_size(price_history) + record['size']
            self.incomplete_aggregated[line_side][
                price][timestamp] = size


    def remove_from_aggregated(self, order):
        """Dealing with 'done' messages"""

        # Discard 'filled' messages because already managed by
        # self.match_aggregated() and not tracked (i.e. no 'open'
        # message present in messages list) orders
        if order['reason'] == 'filled' or \
           not(self.order_tracked(order, 'done')):
            return

        # Define variables
        record = {
            'side': order['side'],
            'price': order['price'],
            'size': float(order['remaining_size']),
            'time': order['time']
            }
        side = record['side']
        price = record['price']
        timestamp = record['time']

        # Get item in self.timeline with same side and price,
        # should always be present
        if side == 'buy':
            line_side = 'bids'
            price_history = self.aggregated_bid_history(price)
        else:
            line_side = 'asks'
            price_history = self.aggregated_ask_history(price)
            
        if price_history: # should always happen
            # subtract from most recent size
            size = self.get_size(price_history) - record['size']
            self.incomplete_aggregated[line_side][
                price][timestamp] = size


    def match_aggregated(self, order):
        """Dealing with match message"""

        # Discard matches involving untracked orders
        if not(self.order_tracked(order, 'match')):
            return

        # Define variables
        record = {
            'side': order['side'],
            'price': order['price'],
            'size': float(order['size']),
            'time': order['time']
            }
        side = record['side']
        price = record['price']
        timestamp = record['time']

        # Get item with same side and price
        if side == 'buy':
            line_side = 'bids'
            price_history = self.aggreagated_bid_history(price)
        else:
            line_side = 'asks'
            price_history = self.aggreagated_ask_history(price)
            
        # Subtract match size from most recent price update
        if price_history:
            size = self.get_size(price_history) - record['size']
            self.incomplete_aggregated[line_side][
                price][timestamp] = size


    def change_aggregated(self, order):
        """ Dealing with 'change' orders """
        
        # Discard messages without 'new_size' key (should be market) 
        try:
            order.get('new_size')
        except KeyError:
            return
        # Discard untracked orders
        if not(self.order_tracked(order, 'change')):
            return

        # Define variables
        record = {
            'side': order['side'],
            'price': order['price'],
            'size': None,
            'time': order['time']
            }
        side = record['side']
        price = record['price']
        timestamp = record['time']

        # Get item with same side and price
        if side == 'buy':
            line_side = 'bids'
            price_history = self.aggregated_bid_history(price)
        else:
            line_side = 'asks'
            price_history = self.aggregated_ask_history(price)

        # Subtract size change from most recent size update
        # of that price
        if price_history:
            diff_size = (float(order['old_size']) -
                         float(order['new_size']))
            size = self.get_size(price_history) - diff_size
            self.incomplete_aggregated[line_side][
                price][timestamp] = size


    def aggregated_bid_history(self, price):
        """
        Return item of 'bids' dict with given price, None if not any

        """
        
        bids_dict = self.incomplete_aggregated['bids']
        if price in bids_dict:
            bid_history = bids_dict.get(price)
            return bid_history
        else:
            return None


    def aggregated_ask_history(self, price):
        """
        Return item of 'asks' dict with given price, None if not any

        """
        
        asks_dict = self.incomplete_aggregated['asks']
        if price in asks_dict:
            ask_history = asks_dict.get(price)
            return ask_history
        else:
            return None


    def get_size(self, price_history):
        """ Return size of most recent update of given price item"""
        
        last_update = max(list(price_history.keys()))
        return float(price_history[last_update])


    def order_tracked(self, order, msg_type):
        """ 
        Find if present a 'open' message with same id of 
        the given order

        """
        # match messages have maker_order_id instead of order_id
        if msg_type == 'match':
            order_id = 'maker_order_id'
        else:
            order_id = 'order_id'

        # search for open messages
        for message in self.messages:
            if message['type'] == msg_type:
                if message[order_id] == order[order_id] and \
                   message['type'] == 'open':
                    return True
        return False


    def single_orders(self):
        """
        Return a list of tuples (start, end , size, price, side)
        representing time interval where a buy/sell limit order
        with volume 'size' and price 'price' was on the order book.

        No ID is associated with orders.
        Start, end given in isoformat format

        """

        dict_by_id = self.create_dict_by_id()
        # Loop over order ids
        order_ids = list(dict_by_id.keys())
        tuples = []
        for order_id in order_ids:
            messages_by_id = dict_by_id[order_id]
            sorted_messages = self.track_messages_by_id(messages_by_id)
            if not sorted_messages:
                continue
            else:
                self.append_tuples(tuples, sorted_messages)
        return tuples


    def create_dict_by_id(self):
        """
        Return a dict: keys=order ID, values=messages concerning
        orders with 'key' ID

        """
        dict_by_id = defaultdict(list)
        for message in self.messages:
            message_id = (message.get('order_id') or 
                          message.get('maker_order_id'))
            dict_by_id[message_id].append(message)
        return dict_by_id


    def track_messages_by_id(self, message_list):
        """
        From the list of all messages associated with same ID 
        Return a list of tuples (time, price, size, side, msg_type)
        ordered by time, None if open message not present in list,
        i.e. untrackable.

        Notice that list without done message (order still in 
        order book) are tracked. To check better

        """
        
        open_found = False  # to check if open message in list
        ordered_messages = []
        for message in message_list:
            msg = message['type']
            price = message.get('price')
            side = message['side']
            
            # define 'size' variable by message type (see gdax docs)
            if msg == 'received':
                continue
            if msg == 'open':
                size = message.get('remaining_size')
                open_found = True
            if msg == 'match':
                size = message.get('size')
            if msg == 'change':
                size = message['old_size'] - message['new_size']
            if msg == 'done':
                size = message.get('remaining_size')
            # append proper tuple
            ordered_messages.append((message['time'], price,
                                     size, side, msg))
        ordered_messages = sorted(ordered_messages) \
                           if open_found else None
        return ordered_messages
            

    def append_tuples(self, tuples_list, sorted_messages):
        """
        Append (start, end, size, price, side) tuples built
        from given sorted_messages to a given tuples_list

        Notice that orders still active are not tracked during
        interval (last message time, streaming end time). 
        Maybe change this

        """
        
        previous_order = sorted_messages.pop(0)
        for message in sorted_messages:
            start = previous_order[0]
            end = message[0]
            price = previous_order[1]
            size = float(previous_order[2])
            side = previous_order[3]
            if message[4] == 'done': # remaining_size is null
                size_change = size # subtract all money from book
            else: # match or change message
                size_change = float(message[2])
            # store event as tuple
            tuples_list.append((start, end, price, size, side))
            previous_order = (end, price, size - size_change, side)


import pickle
import datetime as dt
from settings.constants import Market, CurrencyPair
from bintrees import RBTree
from decimal import Decimal
from gdax import PublicClient, WebsocketClient


class OrderBookNew(WebsocketClient):
    def __init__(self, product_id='BTC-USD', log_to=None):
        super(OrderBookNew, self).__init__(products=product_id)
        self.market = 'gdax'
        self._asks = RBTree()
        self._bids = RBTree()
        self._client = PublicClient()
        self._sequence = -1
        self._log_to = log_to
        if self._log_to:
            assert hasattr(self._log_to, 'write')
        self._current_ticker = None
        self.start_time = None
        self.stop_time = None

    @property
    def product_id(self):
        ''' 
        Currently OrderBook only supports a single product 
        even though it is stored as a list of products
        '''
        return self.products[0]

    def on_open(self):
        self._sequence = -1
        self.start_time = dt.datetime.now()
        print("-- Subscribed to OrderBook! --\n")

    def on_close(self):
        self.stop_time = dt.datetime.now()
        print("\n-- OrderBook Socket Closed! --")

    def reset_book(self):
        self._asks = RBTree()
        self._bids = RBTree()
        res = self._client.get_product_order_book(
            product_id=self.product_id, level=3)
        for bid in res['bids']:
            self.add({
                'id': bid[2],
                'side': 'buy',
                'price': Decimal(bid[0]),
                'size': Decimal(bid[1])
            })
        for ask in res['asks']:
            self.add({
                'id': ask[2],
                'side': 'sell',
                'price': Decimal(ask[0]),
                'size': Decimal(ask[1])
            })
        self._sequence = res['sequence']

    def on_message(self, message):
        if self._log_to:
            pickle.dump(message, self._log_to, protocol=2)

        sequence = message['sequence']
        if self._sequence == -1:
            self.reset_book()
            return
        if sequence <= self._sequence:
            # ignore older messages(e.g. before order book
            # initialization from getProductOrderBook)
            return
        elif sequence > self._sequence + 1:
            self.on_sequence_gap(self._sequence, sequence)
            return

        msg_type = message['type']
        if msg_type == 'open':
            self.add(message)
        elif msg_type == 'done' and 'price' in message:
            self.remove(message)
        elif msg_type == 'match':
            self.match(message)
            self._current_ticker = message
        elif msg_type == 'change':
            self.change(message)

        self._sequence = sequence

    def on_sequence_gap(self, gap_start, gap_end):
        self.reset_book()
        print('Error: messages missing ({} - {}).' + 
              'Re-initializing  book at sequence.'.format(
                  gap_start, gap_end, self._sequence))


    def add(self, order):
        order = {
            'id': order.get('order_id') or order['id'],
            'side': order['side'],
            'price': Decimal(order['price']),
            'size': Decimal(order.get('size') or
                            order['remaining_size'])
        }
        if order['side'] == 'buy':
            # get all bids at same order price
            bids = self.get_bids(order['price'])
            if bids is None:
                # first bid at order price
                bids = [order]
            else:
                # we append at list of other bids with this price
                bids.append(order)
            # build aggregated(?) bids 
            self.set_bids(order['price'], bids)
        else:
            # get all asks at same order price
            asks = self.get_asks(order['price'])
            if asks is None:
                asks = [order]
            else:
                asks.append(order)
            # build aggregated (?) asks
            self.set_asks(order['price'], asks)

    def remove(self, order):
        price = Decimal(order['price'])
        if order['side'] == 'buy':
            # get all bids at same order price
            bids = self.get_bids(price)
            if bids is not None:
                # if other bids present at that price
                # we remove from bids list
                bids = [o for o in bids if o['id'] != order['order_id']]
                if len(bids) > 0: # if others left after remove
                    self.set_bids(price, bids) # aggregated
                else: # if it was the last bid at that price
                    self.remove_bids(price)
        else: # same of 'buy' side but for sell
            asks = self.get_asks(price)
            if asks is not None:
                asks = [o for o in asks if o['id'] != order['order_id']]
                if len(asks) > 0:
                    self.set_asks(price, asks)
                else:
                    self.remove_asks(price)

    def match(self, order):
        size = Decimal(order['size'])
        price = Decimal(order['price'])

        if order['side'] == 'buy':
            # get all bids at that price
            bids = self.get_bids(price)
            if not bids: # TODO: check how this can happen
                return
            # we assert matches happen with orders on top of bids list 
            assert bids[0]['id'] == order['maker_order_id']
            if bids[0]['size'] == size:
                # if size of trade is the same of order size
                # we remove that order (we expect a 'done' message then)
                # and update aggregated bids
                self.set_bids(price, bids[1:])
            else:
                # we subtract match size from order size
                # and update aggregated
                bids[0]['size'] -= size
                self.set_bids(price, bids)
        else: # same of bids but for asks
            asks = self.get_asks(price)
            if not asks:
                return
            assert asks[0]['id'] == order['maker_order_id']
            if asks[0]['size'] == size:
                self.set_asks(price, asks[1:])
            else:
                asks[0]['size'] -= size
                self.set_asks(price, asks)

    def change(self, order):
        try: # just relevant changes
            new_size = Decimal(order['new_size'])
        except KeyError:
            return

        try: # just relevant changes
            price = Decimal(order['price'])
        except KeyError:
            return

        if order['side'] == 'buy':
            # get all bids at that price
            bids = self.get_bids(price)
            # check how following condition can happen
            if bids is None or not any(o['id'] == order['order_id']
                                       for o in bids):
                return
            # find order in bids list and set 'new_size' as size
            index = [b['id'] for b in bids].index(order['order_id'])
            bids[index]['size'] = new_size
            # update aggregated
            self.set_bids(price, bids)
        else: # same of bids but for asks
            asks = self.get_asks(price)
            if asks is None or not any(o['id'] == order['order_id']
                                       for o in asks):
                return
            index = [a['id'] for a in asks].index(order['order_id'])
            asks[index]['size'] = new_size
            self.set_asks(price, asks)

        tree = self._asks if order['side'] == 'sell' else self._bids
        node = tree.get(price)

        if node is None or not any(o['id'] == order['order_id'] for o in node):
            return

    def get_current_ticker(self):
        return self._current_ticker

    def get_current_book(self):
        result = {
            'sequence': self._sequence,
            'asks': [],
            'bids': [],
        }
        for ask in self._asks:
            try:
                # There can be a race condition here,
                # where a price point is removed between these two ops
                this_ask = self._asks[ask]
            except KeyError:
                print('KeyError')
                continue
            for order in this_ask:
                result['asks'].append([order['price'],
                                       order['size'], order['id']])
        for bid in self._bids:
            try:
                # There can be a race condition here,
                # where a price point is removed between these two ops
                this_bid = self._bids[bid]
            except KeyError:
                continue

            for order in this_bid:
                result['bids'].append([order['price'],
                                       order['size'], order['id']])
        return result

    def get_ask(self):
        return self._asks.min_key()

    def get_asks(self, price):
        return self._asks.get(price)

    def remove_asks(self, price):
        self._asks.remove(price)

    def set_asks(self, price, asks):
        self._asks.insert(price, asks)

    def get_bid(self):
        return self._bids.max_key()

    def get_bids(self, price):
        return self._bids.get(price)

    def remove_bids(self, price):
        self._bids.remove(price)

    def set_bids(self, price, bids):
        self._bids.insert(price, bids)




class OrderBookConsole(OrderBookNew):
    ''' Logs real-time changes to the bid-ask spread to the console '''
    
    def __init__(self, product_id=None):
        super(OrderBookConsole, self).__init__(product_id=product_id)
        
        # latest values of bid-ask spread
        self._bid = None
        self._ask = None
        self._bid_depth = None
        self._ask_depth = None

    def on_message(self, message):
        super(OrderBookConsole, self).on_message(message)

        # Calculate newest bid-ask spread
        bid = self.get_bid()
        bids = self.get_bids(bid)
        bid_depth = sum([b['size'] for b in bids])
        ask = self.get_ask()
        asks = self.get_asks(ask)
        ask_depth = sum([a['size'] for a in asks])

        if (self._bid == bid and
            self._ask == ask and
            self._bid_depth == bid_depth and
            self._ask_depth == ask_depth):
            # If there are no changes to the bid-ask spread
            # since the last update, no need to print
            pass
        else:
            # If there are differences, update the cache
            self._bid = bid
            self._ask = ask
            self._bid_depth = bid_depth
            self._ask_depth = ask_depth
            print('{} {} bid: {:.3f} @ {:.2f}\task: {:.3f} @ {:.2f}'.format(
                dt.datetime.now(), self.product_id,
                bid_depth, bid, ask_depth, ask))


class GDAXQueuer(OrderBookNew):
    def __init__(self, message_queue, product_id='BTC-USD'):
        super(GDAXQueuer, self).__init__(product_id=product_id)
        self.queue = message_queue
        self.currency_pair_index = self.get_currency_pair_index(
            product_id)
        self.market_index = Market.gdax.value

    def on_message(self, message):
        item = {
            "market": self.market_index,
            "currency_pair": self.currency_pair_index,
            "message": message
        }
        self.queue.put(item)

    def get_currency_pair_index(self, product_id):
        currency_pair = product_id[:3].lower() + '_' + \
                        product_id[-3:].lower()
        return CurrencyPair[currency_pair].value

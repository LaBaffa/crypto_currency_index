from gdax_stream.order_book_new import GDAXQueuer
from gdax_stream.gdax_workers import gdax_timeliner
import queue
from utils.io import LOADER
from settings.constants import gdax_product_ids


PATH  = 'gdax_tuples_data.pickle'


if __name__ == '__main__':
    gdax_queue = queue.Queue()
    gdax_book = {}
    timeline = []
    # create a stream for each currency pair
    streams = []
    for product in gdax_product_ids:
        stream = GDAXQueuer(gdax_queue, product_id=product)
        streams.append(stream)
    # start streams
    for stream in streams:
        stream.start()
    f = open(PATH, 'wb')
    try:
        while True:
            gdax_timeliner(gdax_queue, gdax_book, timeline, f)
    except KeyboardInterrupt:  # close (or try to) queue and streams
        gdax_queue.join()
        f.close()
        for stream in streams:
            stream.close()

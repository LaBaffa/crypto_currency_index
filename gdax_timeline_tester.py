# test of GDAXTimeline class

from gdax_stream.gdax_timeline import GDAXTimeline
import pickle

# open gdax data file
gdax_file = open('gdax_data_test.pkl', 'rb')
gdax_messages = [] # list of messages from gdax

# Extract data from file and append to our list
while True:
    try:
        gdax_messages.append(pickle.load(gdax_file))
    except EOFError:
        break


# create aggregated and single_order timelines
timeline = GDAXTimeline(gdax_messages)
aggregated_dict = timeline.aggregated_orders()
single_tuples = timeline.single_orders()

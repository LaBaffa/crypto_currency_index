from multiprocessing import Process, Queue, Event
from Queue import Empty
import time

"""
Python's multiprocessing is a pile of buggy mess. Implementing proper shutdown sequence is quite tricky.
Be sure to read, the following sources before even trying to change something.
It is very important to use blocking operations (like Queue.get()) with timeouts. Otherwise they become
uninterruptable ([2,3]).

Also, queue.put() can block and it may prevent program from exiting, be sure to read [4].
And no, cancel_join_thread() does not help :(.

References:
[1] http://stackoverflow.com/questions/11312525/catch-ctrlc-sigint-and-exit-multiprocesses-gracefully-in-python
[2] http://stackoverflow.com/questions/19652446/python-program-with-thread-cant-catch-ctrlc
[3] http://bugs.python.org/issue1167930
[4] https://docs.python.org/2/library/multiprocessing.html#programming-guidelines
"""

QUEUE_GET_TIMEOUT = 0.5


class PoolCancelledException(Exception):
    pass


class CCIPool:
    def __init__(self, target, *extra_args, **kwargs):
        self.tasks_queue = Queue()
        self.results_queue = Queue()
        self.cancel_event = Event()
        self.target_args = (self.tasks_queue, self.results_queue, self.cancel_event) + extra_args
        self.num_workers = kwargs.get('num_workers', 3)
        self.pool_cancelled = False

        self.processes = []
        for i in range(self.num_workers):
            process = Process(target=target, args=self.target_args)
            process.start()
            self.processes.append(process)

    def dictmap(self, d):
        """
        Extends the map method to handle iteration over dictionary items. 

        :param d: input dictionary
        :return: result dictionary 
        """
        if not d:
            return {}

        if self.cancel_event.is_set():
            raise PoolCancelledException("It's dead, Alice")

        for k, v in d.iteritems():
            self.tasks_queue.put((k, v))

        results = {}
        while True:
            try:
                key, res = self.results_queue.get(timeout=QUEUE_GET_TIMEOUT)
                results[key] = res
                if len(results) == len(d):
                    break
            except Empty:
                if self.cancel_event.is_set():
                    raise PoolCancelledException
        return results

    def cancel(self):
        """
        Helps terminating the pool execution in a graceful way.
        The pool is _unusable_ after that.
        """
        self.cancel_event.set()

    def is_cancelled(self):
        return self.cancel_event.is_set()

    def join(self):
        """ Waits workers to finish. Also ensures all the cleanups
            needed for clean termination of program.
        """
        self.cancel_event.wait()

        while True:
            # keep flushing queues until all workers quit
            if not any(p.is_alive() for p in self.processes):
                break
            self._flush_queues()
            time.sleep(0.1)
        # after all workers quit we must be sure there is no data in queues
        # or multiprocessing atexit handlers will stuck forever
        self._flush_queues()

    def _flush_queues(self):
        flush_queue(self.tasks_queue)
        flush_queue(self.results_queue)


def flush_queue(q):
    while True:
        try:
            q.get_nowait()
        except Empty:
            break

import pusherclient  # pip install pusherclient
import json
import time
import pickle
from settings.constants import  Market, CurrencyPair, \
    bitstamp_currency_pairs
from bitstamp.bitstamp_workers import process_bid, process_ask


SECONDS_OFFSET = 3600  # bitstamp time is  UTC +1

# Set a dict of all base available channels from bitstamp streaming
bitstamp_base_channels = {
    'live_trades': ['trade'],
    'order_book': ['data'],
    'diff_order_book': ['data'],
    'live_orders': ['order_created',
                    'order_changed',
                    'order_deleted']}

bitstamp_extended_channels = {}
# Extend base channels to all available currency pairs
for channel in list(bitstamp_base_channels.keys()):
    # 'currency_pair' channels have same event_name of their base 
    events = bitstamp_base_channels.get(channel)
    # every channel can stream from all available currency pairs
    for currency_pair in bitstamp_currency_pairs:
        # for each currency pair we add channels and events to the dict
        currency_pair_channel = channel + '_' + currency_pair
        bitstamp_extended_channels[currency_pair_channel] = events



def is_default_pair(channel):
    if channel in bitstamp_base_channels:
        return True
    return False


class BitstampEventConnector(object):
    """ 
    Class to connect to a single event of Bitstamp Pusher object.
    Channel and event are mandatory.

    Pass a file pointer to 'log_to' arg to write events on file

    """
    def __init__(self, channel='diff_order_book', event='data',
                 log_to=None):
        self.app_key = "de504dc5763aeef9ff52"  # bitstamp key
        self.pusher = pusherclient.Pusher(self.app_key)
        self.channel = channel
        self.channel_streamer = None
        self.events = []
        self.event = event
        self._log_to = log_to
        if self._log_to:
            assert hasattr(self._log_to, 'write')
        self.stop = False


    def start(self):
        """ Connect to bitstamp Pusher object """
        
        def _go(data):
             # call _listen() once connected
            self._listen()
            
        self.stop = False
        self.pusher.connect()
        self.pusher.connection.bind('pusher:connection_established',_go)


    def _listen(self):
        """ 
        Subscribe to self.channel and call on_message once bound 
        to self.event

        """
        self.channel_streamer = self.pusher.subscribe(self.channel)
        self.channel_streamer.bind(self.event, self.on_message)


    def on_message(self, data):
        """ 
        Write a dict version of the message on file if log_to passed.
        Else do nothing

        """
        event = json.loads(data)
        event['type'] = self.event
        event['channel'] = self.channel
        if self._log_to:
            pickle.dump(event, self._log_to, protocol=2)        
            # self.events.append(event)

    def on_open():
        """ Something to do while opening """
        pass


    def on_close(self):
        """ Something to do once disconnected"""
        pass


    def close(self):
        """Disconnect from bitstamp Pusher object and call on_close()"""
        
        self.stop = True
        self.pusher.disconnect()
        self.on_close()
        time.sleep(1)


class BitstampStreamer(object):
    """ 
    Class to stream from desired channels of bitstamp
    Pusher object at the same time.
    
    Channels should be passed comma separated in the same string.
    Example: BitstampStreamer(channels='live_orders,live_trades_ethusd')
             will stream all events of live_orders (btc-usd) and
             live_trades for eth-usd pair
    channels='all' to connect to all channels  

    Pass a file pointer to log_to arg to write messages on file
    
    """
    
    def __init__(self, channels='live_orders', log_to=None):
        self.market = 'bitstamp'
        self.channels = self.set_channels(channels)
        self._log_to = log_to
        self.connectors = []

    def set_channels(self, channels):
        """Parse channel argument and set self.channels"""
        
        if not channels:
            self.channels = ['live_orders']
        elif channels == 'all':
            self.channels = list(bitstamp_channels.keys())
        else:
            channels.replace(' ', '')
            self.channels = channels.split(',')
        return self.channels

    def connect_to_events(self):
        """ Connect to all events of all desired channel"""
        
        for channel in self.channels:
            for event in bitstamp_channels[channel]:
                self.connectors.append(
                    BitstampEventConnector(channel, event,
                                           log_to=self._log_to))

    def start(self):
        """ Connect and start streaming """
        
        self.connect_to_events()
        for connector in self.connectors:
            connector.start()

    def close(self):
        "Stop streaming from all events """
        
        for connector in self.connectors:
            connector.close()


class BitstampQueuer(BitstampEventConnector):
    """ Queue messages on a queue

    It works just with 'diff_order_book' channels.
    Use set_currency_pair() method or pass via channel to 
    currency pairs different from 'btcusd'

    """
    def __init__(self, message_queue,
                 channel='diff_order_book', event='data'):
        super(BitstampQueuer, self).__init__(channel=channel,
                                             event=event)
        self.market_index = Market.bitstamp.value
        self.currency_pair_index = self.get_currency_pair_index()
        self.queue = message_queue

    def set_currency_pair(self, pair):
        """ Use 'abcdef' format (e.g.'btceur')"""
        
        if pair == 'btcusd':
            self.channel = 'diff_order_book'
            self.currency_pair_index = CurrencyPair["btc_usd"].value
            return
        self.channel = 'diff_order_book' + '_' + pair
        self.currency_pair_index = self.get_currency_pair_index()

    def get_currency_pair_index(self):
        if is_default_pair(self.channel):
            return CurrencyPair["btc_usd"].value
        currency_pair = self.channel.split('_')[-1]
        currency_pair = currency_pair[:3].lower() + '_' + \
                        currency_pair[-3:].lower()
        return CurrencyPair[currency_pair].value


    def on_message(self, message):
        
        message = json.loads(message)
        bids = message['bids']
        asks = message['asks']
        timestamp = int(message["timestamp"])
        for bid in bids:
            item = {
                "market": self.market_index,
                "currency_pair": self.currency_pair_index,
                "time": timestamp - SECONDS_OFFSET,
                "message": bid
            }
            self.queue.put((process_bid, item))
        for ask in asks:
            item = {
                "market": self.market_index,
                "currency_pair": self.currency_pair_index,
                "time": timestamp - SECONDS_OFFSET,
                "message": ask
            }            
            self.queue.put((process_ask, item))
            

import queue
import time
import json
from settings.constants import Side, Market, CurrencyPair
from utils.io import DUMPER



def bitstamp_timeliner(queue, order_book, timeline, file_obj):
    process, item = queue.get()
    process(item, order_book, timeline, file_obj)
    queue.task_done()


def process_bid(item, order_book, timeline, file_obj):
    result = None
    bid_info = build_info(item, 'buy')
    pair = bid_info[-1]
    pair_book = order_book[pair]
    same_price_on_book = find_price_on_side(bid_info, pair_book)
    if same_price_on_book:
        time_from_last_update = item['time']!=same_price_on_book['time']
        if time_from_last_update:
            result = append_processed_price(bid_info,
                                            same_price_on_book,
                                            timeline)
            DUMPER(file_obj, result)
        update_price_on_book(bid_info, pair_book)
    if not same_price_on_book:
        size = bid_info[1]
        if size > 0:
            insert_price_in_book(bid_info, pair_book)
    return result


def build_info(item, side):
    """
    Return (price, size, timestamp, side, market, pair) from item

    """
    price = float(item['message'][0])
    size = float(item['message'][1])
    side = Side[side].value
    market = item['market']
    pair = item['currency_pair']
    timestamp = item['time']
    return (price, size, timestamp, side, market, pair)


def process_ask(item, order_book, timeline, file_obj):
    result = None
    ask_info = build_info(item, 'sell')
    pair = ask_info[-1]
    pair_book = order_book[pair]
    same_price_on_book = find_price_on_side(ask_info, pair_book)
    if same_price_on_book:
        time_from_last_update = item['time']!=same_price_on_book['time']
        if time_from_last_update:
            result = append_processed_price(ask_info,
                                            same_price_on_book,
                                            timeline)
            DUMPER(file_obj, result)
        update_price_on_book(ask_info, pair_book)
    if not same_price_on_book:
        size = ask_info[1]
        if size > 0:
            insert_price_in_book(ask_info, pair_book)
    return result




def find_price_on_side(new_info, order_book):
    """ 
    Return last update of given price and side if any, else None 

    new_info = (price, size, timestamp, side, market, pair)
    """
    side = new_info[3]
    price = new_info[0]
    side_prices = order_book[side]
    if price in side_prices:
        return side_prices[price]
    return None


def update_price_on_book(new_info, order_book):
    """
    Substitute new_info price with new_info infos on given order book

    """
    side = new_info[3]
    side_prices = order_book[side]
    price = new_info[0]
    size = new_info[1]
    if not size:
        del side_prices[price]
        return
    timestamp = new_info[2]
    side_prices[price] = {
        "size": size,
        "time": timestamp
        }


def insert_price_in_book(new_info, order_book):
    """
    order_book keys:
    - 'bids': bid prices with non-zero size
    - 'asks': ask prices with non-zero size
    prices keys: 
    - size: current size of bids at that price
    - time: time of the last size change

    """
    side = new_info[3]
    side_prices = order_book[side]
    price = new_info[0]
    size = new_info[1]
    timestamp = new_info[2]
    side_prices[price] = {
        "size": size,
        "time": timestamp
        }


def append_processed_price(new_info, old_info, timeline):
    """ 
    Append (start, end, price, size, side, market) to timeline

    """
    start = old_info['time']
    end = new_info[2]
    price = new_info[0]
    size = old_info['size']
    side = new_info[3]
    market = new_info[4]
    pair = new_info[5]
    new_element = (start, end, price, size, side, market, pair)
    timeline.append(new_element)
    return new_element

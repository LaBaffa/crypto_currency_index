import pusherclient  # pip install pusherclient
import json
import time

   
class ChannelConnector():
    
    def __init__(self, seconds, channel_name, event_name):
        """
        Connect to bitstamp Pusher object for given seconds.
        Subcribe to 'channel_name' and bind to 'event_name' when 
        connection is established.

        CTRL+C to quit connection before time
        
        """
        
        def connection_handler(data):
            """ 
            Call event_handler when bound to the event_name of the
            given channel_name

            """
            channel = pusher.subscribe(channel_name)
            channel.bind(event_name, event_handler)
            
        def event_handler(data):
            """ Append events as dicts to the self.event list """
            event = json.loads(data)
            self.events.append(event)

        self.events = []  # list of received events
        app_key = "de504dc5763aeef9ff52"  # bitstamp key
        
        # Connect to bitstamp...
        pusher = pusherclient.Pusher(app_key)
        pusher.connect()
        # ...and call 'connection_handler' once connected
        pusher.connection.bind('pusher:connection_established',
                               connection_handler)

        # Disconnect when given 'seconds' elapsed or on CTRL+C
        time_out = time.time() + seconds
        try:
            while True:
                if time.time() > time_out:
                    pusher.disconnect()
                    time.sleep(1)
                    break
        except KeyboardInterrupt:
            pusher.disconnect()
            time.sleep(1)
            pass



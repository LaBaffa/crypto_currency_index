from bitstamp.bitstamp_streamer import BitstampQueuer
from bitstamp.bitstamp_workers import bitstamp_timeliner
import queue
from utils.io import LOADER
from settings.constants import bitstamp_currency_pairs, CurrencyPair


PATH = 'bitstamp_tuple_data.pickle'


if __name__ == '__main__':
    bitstamp_queue = queue.Queue()
    timeline = []
    bitstamp_book = {}
    # create a stream for each currency pair    
    total_currency_pairs = list(bitstamp_currency_pairs)
    total_currency_pairs.append('btcusd')
    streams = []
    for pair in total_currency_pairs:
        stream = BitstampQueuer(bitstamp_queue)
        stream.set_currency_pair(pair)
        # create a dict for each currency_pair_index
        pair_index = stream.get_currency_pair_index()
        streams.append(stream)
        bitstamp_book[pair_index] = {True: {}, False: {}}
    # start streams    
    for stream in streams:
        stream.start()
    f = open(PATH, 'wb')
    try:
        while True:
            bitstamp_timeliner(bitstamp_queue, bitstamp_book,
                               timeline, f)
    except KeyboardInterrupt:  # close (or try to) queue and streams 
        bitstamp_queue.join()
        f.close()
        for stream in streams:
            stream.close()

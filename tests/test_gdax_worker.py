from gdax_stream.gdax_workers import unix_from_isoformat


UNIX_START_TIME = "1970-01-01T00:00:01.000Z"


def test_datetime_converter():
    assert unix_from_isoformat(UNIX_START_TIME) == 1

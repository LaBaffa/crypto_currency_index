import pickle

def LOADER(path):
    loaded_data = []
    with open(path, 'rb') as fd:
        while True:
            try:
                loaded_data.append(pickle.load(fd))
            except EOFError:
                break
    return loaded_data


def DUMPER(file_obj, result):
    pickle.dump(result, file_obj, protocol=2)  # python2.7

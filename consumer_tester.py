from gdax_stream.order_book_new import OrderBookNew
from bitstamp.bitstamp_streamer import BitstampStreamer
from bot_classes.markets_consumer import MarketsConsumer
import time

stream = MarketsConsumer(OrderBookNew(product_id='BTC-USD'),
                         BitstampStreamer(channels='diff_order_book'))
stream.start()
try:
    while True:
        time.sleep(10)
except KeyboardInterrupt:
    stream.close()

import argparse
from pool import CCIPool, Empty, PoolCancelledException


def ensure_positive_int(value):
    value = int(value)
    if value <= 0:
        raise argparse.ArgumentTypeError("Number of workers should be positive: %s" % value)
    return value


def parse_arguments():
    parser = argparse.ArgumentParser(description='Process market-stream messages')
    parser.add_argument('-w', '--num-workers', metavar='N', type=ensure_positive_int, default=4,
                        help='Number of worker subprocesses in the pool')
    parser.add_argument('-1', '--one-shot', action='store_true',
                        help='Exit as soon as the first message is processed (for automated testing)')
    args = parser.parse_args()
    return args


def worker(tasks_queue, results_queue, cancel_event):
    pass


def main():
    args = parse_arguments()
    # create pool of processes, here worker is the target function
    pool = CCIPool(worker, num_workers=args.num_workers)

    previous_result = {}
    while True:
        # do not try to process anything if pool processing was cancelled
        if pool.is_cancelled():
            break
        try:
            iterable = ['stream1', 'stream2']
            # Here the dictmap method of the pool will populate the queues with the elements of iterable
            result = pool.dictmap(iterable)
            previous_result = result
        except PoolCancelledException:
            break

    # gracefully shutdown workers
    pool.cancel()  # this will never hurt (c) Slackware
    pool.join()    # wait till workers gracefully finish processing, only then quit

if __name__ == '__main__':
    main()

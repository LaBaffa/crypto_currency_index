import enum

DATE_FMT = "%Y-%m-%dT%H:%M:%S.%fZ"

gdax_product_ids = ['BCH-USD', 'LTC-EUR', 'LTC-USD', 'LTC-BTC',
                    'ETH-EUR', 'ETH-USD', 'ETH-BTC', 'BTC-GBP',
                    'BTC-EUR', 'BTC-USD']

bitstamp_currency_pairs = ['btceur', 'eurusd', 'xrpusd', 'xrpeur',
                          'xrpbtc', 'ltcusd', 'ltceur', 'ltcbtc',
                          'ethusd', 'etheur', 'ethbtc', 'bchusd',
                          'bcheur', 'bchbtc']


all_currency_pair_indexes = {
    "btc_usd": 1, "btc_eur": 2, "btc_gbp": 3,
    "eth_usd": 4, "eth_eur": 5, "eth_btc": 6,
    "bch_usd": 7, "bch_eur": 8, "bch_btc": 9,
    "ltc_usd": 10, "ltc_eur": 11, "ltc_btc": 12,
    "xrp_usd": 13, "xrp_eur": 14, "xrp_btc": 15,
    "eur_usd": 16
    }


CurrencyPair = enum.Enum("CurrencyPair", all_currency_pair_indexes)


class Side(enum.Enum):
    buy = True
    sell = False


class Market(enum.Enum):
    gdax = 1
    bitstamp = 2

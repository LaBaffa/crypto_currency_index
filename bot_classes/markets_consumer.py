class MarketsConsumer(object):
    """ 
    Class to stream from different markets at once

    Can pass a variable number of 'connectors'.
    A connector is an object able to connect to a market and must 
    have a start() and a close() method, a _log_to and 
    a 'market' attribute

    """
    def __init__(self, *connectors):
        self.market_connectors = [connector for connector in connectors]
        self._log_to = self.set_logs(connectors)

    def start(self):
        """Start all connectors"""
        for connector in self.market_connectors:
            connector.start()

    def set_logs(self, connectors):
        """ 
        Set _log_to for each connector.
        log file will name after market connector

        """
        
        file_pointers = []
        for connector in connectors:
            file_name = str(connector.market) + '.pkl'
            file_pointer = open(file_name, 'wb')
            connector._log_to = file_pointer

    def close(self):
        """Close all connectors """
        
        for connector in self.market_connectors:
            connector.close()

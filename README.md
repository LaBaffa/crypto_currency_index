# Various multiplex multiformis

## Before you start
Activate your virtual environment (if any) and run
```
pip install -r requirements.txt
```

Run tests with
```
python -m pytest -v tests
```
## master branch
To save data locally from data gdax and bitstamp markets as
(t0, t1, price, size, side, market, currency pair) tuples.
```PATH```  at the beginning of the scripts to change file path
```
python bitstamp_tuples.py
python gdax_tuples.py
```

## mysql branch
To save data as tuples from gdax and bitstamp on mySQLdb.
Install mySQL. On Ubuntu you can follow [install](https://support.rackspace.com/how-to/installing-mysql-server-on-ubuntu/).
Create ```user: root``` with ```password: burbu```.
Run mySQL via
```
$ mysql -u root -p
```
copy/paste content of ```mysql_script/create_db.sql``` to mysql command line.
and now run
```
python bitstamp_tuples.py
python gdax_tuples.py
```
